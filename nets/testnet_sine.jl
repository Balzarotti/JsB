## Synapses
inhibitory = Dict("latency" => 4,
                  "intensity" => -20,
                  "learning" => false)
excitatory = Dict("latency" => 4,
                  "intensity" => +20,
                  "learning" => false)

## Neurons
CIN = Dict("type" => "IZ",
           "params" => Dict("a" => 0.05,
                            "b" => 0.21,
                            "c" => -45.1,
                            "d" => 0.35,
                            "s" => 0,
                            "u" => -16,
                            "v" => -72))
EIN = Dict("type" => "IZ",
           "params" => Dict("a" => 0.02,
                            "b" => 0.30,
                            "c" => -65,
                            "d" => 0.04,
                            "s" => 0,
                            "u" => -16,
                            "v" => -72))
MN = Dict("type" => "IZ",
          "params" => Dict("a" => 0.02,
                           "b" => 0.25,
                           "c" => -55,
                           "d" => 0.05,
                           "s" => 0,
                           "u" => -16,
                           "v" => -64))

## Stimuli
sine_input_start = 3000
sine_input_freq = 0.7
SINE_INPUT_C = Dict("type" => "sine",
                  "start" => sine_input_start,
                  "stop" => 30000,
                  "freq" => sine_input_freq,
                  "offset" => 1,
                  "gain" => 3,
                  "ms-shift" => 0,
                  "disabled" => false)
SINE_INPUT_D = Dict("type" => "sine",
                  "start" => sine_input_start,
                  "stop" => 30000,
                  "freq" => sine_input_freq,
                  "offset" => 1,
                  "gain" => 3,
                  "ms-shift" => 0,
                  "disabled" => false)
BOOTSTRAP_A = Dict("type" => "constant",
                   "current" => 20,
                   "start" => 400,
                   "stop" => 440,
                   "disabled" => true)
BOOTSTRAP_B = Dict("type" => "constant",
                   "current" => 20,
                   "start" => 460,
                   "stop" => 500,
                   "disabled" => true)

## The network is a dictionary only, and must evaluate LAST.
## That way, everything else is just inserted in the dict, and lost
## (does not pollute environment)

Dict("name" => "SineCPG_2016-05-17",
     "details" => "Sine wave stimuli example",
     "stimuli" => Dict(1 => [ SINE_INPUT_C ],
                       2 => [ SINE_INPUT_D ],
                       3 => [ BOOTSTRAP_A ],
                       4 => [ BOOTSTRAP_B ]),
     "neurons" => [[ CIN for i in 1:2 ]; ## Numbering is not required
                   [ EIN for i in 3:4 ]; ## but makes read easyer
                   [ MN for i in 5:6 ]],
     "sensory" => Dict(
                       #"reader" => [
                       #             ],
                       "writer" => [Dict("joint" => 1, ## julia indexing
                                         "min" => -90,
                                         "max" => 90,
                                         ## Order matters: from min to max
                                         "neurons" => [5, 6],
                                         "increment" => 1,
                                         "decay" => 0.992)
                                    ]),
     "synapses" => [Dict("from" => 1,
                         "to" => [2, 4],
                         "values" => inhibitory
                         ),
                    Dict("from" => 2,
                         "to" => [1, 3],
                         "values" => inhibitory
                         ),
                    Dict("from" => 2,
                         "to" => [5],
                         "values" => inhibitory
                         ),
                    Dict("from" => 1,
                         "to" => [6],
                         "values" => inhibitory
                         ),
                    Dict("from" => 1,
                         "to" => [2],
                         "values" => inhibitory
                         ),
                    Dict("from" => 3,
                         "to" => [1, 3, 5],
                         "values" => excitatory
                         ),
                    Dict("from" => 4,
                         "to" => [2, 4, 6],
                         "values" => excitatory
                         )
                    ],
     "save-membrane" => Dict("steps" => 1,
                             "neurons" => [1, 2, 6])
     )
