with import <nixpkgs> {};
let nemosim = ( callPackage ./pkgs/nemosim {} );
in stdenv.mkDerivation {
  name = "JsB"; #Julia's Brain
#  nemosim = pkgs.callPackage ./pkgs/nemosim {};
  buildInputs = [
    julia
    git ## julia deps
    curl
    zlib
    pkgconfig
    nemosim
    cudatoolkit
  ];
  LD_LIBRARY_PATH="${nemosim}/lib;/home/nixo/Scm/Git/yarp/dist/lib64/;${cudatoolkit}/lib/stubs/";
  
  shellHook = ''
      unset http_proxy
      export SSL_CERT_FILE=/etc/ssl/certs/ca-bundle.crt
      export GIT_SSL_CAINFO=/etc/ssl/certs/ca-bundle.crt
  '';
}
