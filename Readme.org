How to get JsB working:

* Requisites:

** julia
  =julia 0.4.5=: [[http://julialang.org/]]
  
  NOTE: you can simplify the setup using the =nix= package manager (http://nixos.org/nix/) (every repo used here has a =shell.nix= config file)

** NeMoSim
   NeMo simulator have to be installed
   + Check that =libnemo.so= it is in your =$LD_LIBRARY_PATH= or add it (ie. =/urs/local/lib/libnemo.so=)

** YARP
   YARP too needs to be installed (if you want yarp+gazebo control). It's mandatory enabling the libyarpc compilation (on =2.6.35=, it's disabled by default)
   + You can enable libyarpc compilation using the =cmake= option: =-DYARP_COMPILE_UNMAINTAINED=

   Check that =libyarpc.so= it is in your =$LD_LIBRARY_PATH= or add it (it's in the compiled subfolder =lib64=)

* Install julia bindings:
  from within julia (=$ julia=) run:

** NeMo.jl
#+BEGIN_SRC julia :eval nothing
Pkg.clone("https://github.com/nico202/NeMo.jl")
#+END_SRC

** YARP.jl
#+BEGIN_SRC julia :eval nothing
Pkg.clone("https://github.com/robotology-playground/YARP.jl")
#+END_SRC

** Check that YARP is working
   try to run
#+BEGIN_SRC julia :eval nothing
using YARP
YARP.newnet() # (should return a pointer)
#+END_SRC

** Check that NeMo is working
   try to run
#+BEGIN_SRC julia :eval nothing
using NeMo
NeMo.newnet() #(should return a pointer, too)
#exit from julia
quit()
#+END_SRC

** Clone JsB repo
#+BEGIN_SRC sh
git clone https://gitlab.com/Balzarotti/JsB.git

#+END_SRC

Install dependencies

#+BEGIN_SRC julia
Pkg.add("GZip")
Pkg.add("SHA")
Pkg.add("ArgParse")
Pkg.add("Gadfly")
#+END_SRC

#+BEGIN_SRC sh
cd JsB # Wherever it is
julia --color=yes JsB.jl nets/testnet_sine.jl --output-file=first-test --disable-robot
#+END_SRC

# (note: TRY the first run with the flag "disable-robot" to check that NeMo does work)

# (note: output-file disables the default hash-(SHA1)naming

# (Please note: saving the image the first time the program run is slow. You can see the real speed using a batch)


#+RESULTS:
# INFO: Running a single simulation
# INFO: Cycle 1 of 1
# INFO: Steps: 30000
# INFO: Saving membrane every 1 ms
# INFO: 0.383105993270874 seconds for 30.0s of sim (78.30731057968228x)
# INFO: Saving files to: history/first-test
# WARNING: Not updating history (?useless with output-file defined)!
# INFO: All simulation time was 18.08879061, that's 18.08879061 per simulation

If everything is successful, You should see a new folder (history), containing 4 files:

#+BEGIN_SRC sh :export code
ls history
#+END_SRC

#+RESULTS:
|first-test.membranes.gz| ← binary data, containing the membrane potential. Saving this takes A LOT of time (read period can be customized in the net config file). Can be disabled with the option --no-save-membrane|
|first-test.spikes.gz| ← binary data (Bool array of size steps X neuron number)|
|first-test.stimuli.gz| ← binary data, all the stimuli that were given to the neurons|
|first-test.spikes.svg| ← SVG image of the spikes. This takes much time too|

** Check that batch simulation works:


#+BEGIN_SRC sh
julia --color=yes JsB.jl nets/testnet_batch.jl --disable-robot --save-image-spikes --no-save-membrane
#+END_SRC
+ ProTip: try =julia -O= (optimize) to for a speedup (~160x -> ~180x on my machine)
#+RESULTS:
# Long output...
# INFO: All simulation time was 164.895269723, that's 1.3627708241570249 per simulation
# (goes down to:
# INFO: All simulation time was 32.250128582, that's 0.2665299882809917 per simulation
# disabling image save. Maybe we can reduce the image size?)
# (note: a small overhead is added compressing the output to gz. But the disk space worth it)



+ INFO: Use =julia JsB.jl --help= to see a list of possible cli commands. A useful thing is the ability to save the current cli to a =.json= file, that can be loaded the next time (whitout having to save it in a .sh)

+ Also, nets can be exported/imported as =.json= (the default format is =.jl=). They are more readable (but don't supprot batch).

** Output Data
To read the output data, the file =libs/spikesIO.jl= contains the function =readoutput(filename::ASCIIString)=. It can detect the output file type (if it's a membrane file, spikes, stimuli, angles, jspike output, and if it's gzipped or not). Just try it in the REPL and see the what the file contains.

* Communicate with =YARP=
  If everything went well, we can try the communication with =YARP=.
** Start =yarpserver= in a new terminal window
#+BEGIN_SRC sh
yarpserver --write
#+END_SRC

** Start =gazebo= in a new terminal window
It needs the path to yarp clock library
#+BEGIN_SRC sh
gazebo -slibgazebo_yarp_clock.so extra/single_pendulum.world
#+END_SRC
** Run again
#+BEGIN_SRC sh
julia --color=yes JsB.jl nets/testnet_batch.jl
#+END_SRC
the pendulum should start moving. We're done! (again, check the cli =--help= for info. Watch the output dir for saved files)


# FIXME: cannot find gazebo.jl?

*** Watch the movements!
    If gazebo is working, you should see the signle pendulum. If it's communicating with YARP, it should keep steady up (0°, horizontal)
    
    ProTip: if you need to run many simulation, you can run gazebo headless (replace =gazebo= with =gzserver= in the command above). About 4x times faster. You'll be able (in the future) to watch the simulation AFTER they are run, loading back the commands that has been sent.
    
