#-*- mode: Julia -*-
import GZip

## using Logging
## @Logging.configure(level=DEBUG) ## FIXME: cli

## Custom imports
push!(LOAD_PATH, "./libs")
using cliParse
using spikesIO
import NeMo
import YARP
import jSpike

function main()
    ## Get command line arguments
    cli = parse_commandline()
    ## Check if we need to save the config
    if save_config(cli, cli_defaults)
        ## Run the sim?
        run_or_exit(cli, cli_defaults)
    end
    ## If we're asked to use a saved cli config, load it
    cli = load_cli_conf(cli, cli_defaults)
    ## Adjust outputh path according to new-dir-if-exist
    const outpath = getoutputpath(cli, cli_defaults)
    ## Create the output path, if it does not exists
    mkpath(outpath)

    ## Load the network file. Is a Dict that may contains ranges
    const netconfs::Vector{Dict} = loadnets(cli, cli_defaults)

    ## Number of simulations to run
    const simn::Int = length(netconfs)
    ## If it's a batch, check what loop we are and save to offset
    const isbatch, offset, batchhash = checkbatch_loop(cli, cli_defaults, simn, outpath, netconfs)

    tic() ## Take batch simulations time

    ## TODO: if isbatch && ! hasrobot -> multiprocess
    ## @everywhere for what is needed
    ## @sync @parallel
    firstran::Bool = true
    for (batchcycle, netconf) in enumerate(netconfs[offset:end]) ## Batch
        info("Cycle $(batchcycle+offset-1) of $simn")
        ## One can call the program just to export the network as json
        ## Dies if save and not run
        exportnet_and_die(cli, cli_defaults, netconf)
        
        info("Steps: $(get(cli, "steps", cli_defaults["steps"]))")

        ## START nemo init

        ## Create NeMo network and config (pointers)
        const network, config = NeMo.initnemo()

        const n = NeMo.neuronsadd(network, netconf["neurons"])
        NeMo.synapsesadd(network, netconf["synapses"])

        ## Make synapses write-only
        ## (forbid reads during simulation, speeds up network creation)
        NeMo.synapseswo(config)

        ## END of nemo init
        
        ## Create a new nemo simulation (it's a pointer)
        const sim = NeMo.newsim(network, config)

        
        const steps = get(cli, "steps", cli_defaults["steps"])

        const hasrobot = checkrobot(cli, cli_defaults, netconf)

        if hasrobot
            if firstran ## TODO: move to own function (robotinit)
                firstran = false
                const robotname = get(cli, "robot-name", cli_defaults["robot-name"])
                const clockname = get(cli, "yarp-clock", cli_defaults["yarp-clock"])

                const writername = get(cli, "robot-writer-name", cli_defaults["robot-writer-name"])
                const clockctrl = get(cli, "clock-writer-name", cli_defaults["clock-writer-name"])

                global cron, net, robot, joints, haswriter

                const net = YARP.newnet()
                const robot = YARP.gazebo.connect(net, writername, robotname)
                const cron = YARP.gazebo.connect(net, clockctrl, clockname)

                const joints = YARP.gazebo.getaxes(robot)
                info("$robotname has $joints axes")

                ## TODO: Add reader
                const haswriter = get(netconf["sensory"], "writer", "") != ""
                if haswriter
                    global outputangle, outs
                    global neuronindex, outwriter
                    ## Prepare variables
                    outputangle = 0.0
                    #outputangle::Float64 = 0.0
                    neuronindex = 0
                    #neuronindex::Int = 0
                    #outwriter::Array{Float64} = zeros(joints, steps)
                    outwriter = zeros(joints, steps)
                    ## Create output networks and populate a outs
                    outs = Dict()
                    for grp in netconf["sensory"]["writer"] ## TODO: check for duplicates
                        if ! checkjointnumber(joints, grp)
                            continue
                        end
                        outs[grp["joint"]] = jSpike.NeuronGroupOut(collect(grp["neurons"]),
                                                                   float(grp["min"]),
                                                                   float(grp["max"]),
                                                                   float(grp["increment"]),
                                                                   float(grp["decay"]))
                    end
                end
            end ## firstrun

            info("Resetting robot home position (slowly, can take some time)")
            ## Reset robot position slowly
            for i in 0:joints-1 ## C-like indexing
                assert(YARP.gazebo.setvel(robot, i, 4.))
                ## FIXME: cli home params
                assert(YARP.gazebo.setpos(robot, i, 0.))
            end
            ## Start the world (else checkmotiondone will wait forever)
            YARP.gazebo.start(cron)
            while ! YARP.gazebo.checkmotiondone(robot)
                sleep(0.5)
            end
            ## Reset time
            YARP.gazebo.reset(cron)
            ## Pause the world
            YARP.gazebo.pause(cron)
            assert(YARP.gazebo.setvel(robot, 0, convert(Float64, get(cli, "robot-velocity", cli_defaults["robot-velocity"]))))
            ## Reset time
            if ! get(cli, "no-save-angles", cli_defaults["no-save-angles"])
                outangles::Array{Float64} = zeros(joints, steps) ## FIXME: uninitialized is better 
            end
        end # hasrobot
        
        ## Do we need to save membrane?
        savemembrane, savemembsteps, membraneidxs = save_membrane(cli, cli_defaults, netconf)

        if savemembrane ## FIXME: save to file at every loop
            info("Saving membrane every $savemembsteps ms")
            const totmembsteps = Int(floor(steps / savemembsteps))
            ## FIXME: Faster if uninitialized?
            membraneout = zeros(n, totmembsteps)
        else
            warn("Not saving membrane potential")
        end

        outspikes::Array{Bool}
        outspikes = falses(n, steps) ## Arrays iterated by columns
        stims::Vector{Cuint} = []
        istim::Vector{Float32} = []
        ## Parse the stimuli
        ## TODO: allow macro
        (stimdict, stimsval, minstimms, maxstimms) = stimuli(netconf["stimuli"], n)

        ## Faster to read this than search for key?
        searchstim::Bool = true
        startt::Float64 = time()
        vi = [0]
        ## Main loop
        for ms in 1:steps
            if hasrobot
                angles = YARP.gazebo.angle(robot, 0) ## FIXME: multiple jnts
                ## Save angles
                if get(cli, "no-save-angles", cli_defaults["no-save-angles"])
                    outangles[:, ms] = angles
                end
                asynccall = @spawn YARP.gazebo.step(cron, blocking = true)
                ## TODO: Convert angles to current (jSpike In)
                ## Add to input
            end
            if searchstim && ms > minstimms
                vi = get(stimdict, ms, [0])
            end
            if vi != [0]
                ## Is the last stimuli step?
                if ms > maxstimms
                    searchstim = false
                    break
                end
                msid = ms - minstimms + 1
                stims = vi
                istim = stimsval[:,msid]
                ## istim = slice(stimsval, :, msid) ## Slice doesn't copy array
            else
                istim = []
                stims = []
            end 
            fired::Vector{Int} = NeMo.simstep(sim, n, stims, istim)
            for spike in fired ## BENCH: compare with Dict
                outspikes[n*(ms-1) + fired] = true
            end
            ## Save membrane?
            if savemembrane && ms % savemembsteps == 0
                for memb in NeMo.getmembranes(sim, membraneidxs)
                    membraneout[memb[1], div(ms,savemembsteps)] = memb[2]
                end
            end

            if hasrobot
                wait(asynccall) ## Wait for gazebo step to finish
                ## jSpike: spikes to angle
                for (joint, group) in outs ## Iterate every jSpike output
                    number = length(group.nemoids)
                    firdarr = zeros(Bool, number)
                    neuronindex = 1
                    for nn::Int in group.nemoids
                        if nn in fired
                            firdarr[neuronindex] = true
                        end
                        neuronindex += 1
                    end
                    jSpike.setfirings(group, firdarr)
                    oldoutputangle = outputangle
                    outputangle = jSpike.step(group)
                    outwriter[joint, ms] = outputangle

                    ## FIXME: multiple joints
                    if outputangle != oldoutputangle
                        assert(YARP.gazebo.setpos(robot, joint-1, outputangle))
                    end
                end
            end
        end # simulation
        ## walltime = NeMo.walltime(sim) ## Example call
        elapsed = time() - startt

        simtime = NeMo.simtime(sim)
        ## Verify that NeMo simulation ran the right steps number
        assert(simtime == steps)

        info("$elapsed seconds for $(steps/1000)s of sim ($(steps / 1000 / elapsed)x)")

        ## Start resetting robot position to home while saving
        if hasrobot ## TODO: is really any faster?
            info("Resetting robot home position in background")
            YARP.gazebo.start(cron)
            ## Reset robot position slowly
            for i in 0:joints-1 ## C-like indexing
                assert(YARP.gazebo.setvel(robot, i, 3.))
                ## FIXME: cli home params
                assert(YARP.gazebo.setpos(robot, i, 0.))
            end
        end

        ## Get output name (based on hash/forced naming)
        filename = getname(cli, cli_defaults, netconf)
        dest = "$outpath/$filename"
        ## TODO: Move save to every N steps
        ## (saves up RAM, deferring file write)
        info("Saving files to: $dest")
        writeoutput("$dest.spikes.gz",
                             (convert(UInt32,n),
                              convert(UInt32,steps),
                              outspikes))
        writeoutput("$dest.stimuli.gz",
                             (convert(UInt32,n),
                              convert(UInt32,steps),
                              convert(UInt32,minstimms),
                              convert(UInt32,maxstimms),
                              convert(Array{Float64},stimsval)))
        if hasrobot && ! get(cli, "no-save-angles", cli_defaults["no-save-angles"])
            writeoutput("$dest.angles.gz",
                                 (convert(UInt32,joints),
                                  convert(UInt32,steps),
                                  convert(Array{Float64},outangles)))
        end
        if hasrobot && haswriter
            writeoutput("$dest.writer.gz",
                                 (convert(UInt32,joints),
                                  convert(UInt32,steps),
                                  convert(Array{Float64},outwriter)))
        end
        if savemembrane
            writeoutput("$dest.membranes.gz",
                                 (convert(UInt32,n),
                                  convert(UInt32,totmembsteps),
                                  convert(UInt32,steps),
                                  membraneout))
        end

        #save spikes image
        saveimages(cli, cli_defaults, outspikes, dest, steps)

        updatehistory(cli, cli_defaults, filename, outpath, elapsed, steps, n)
        
        ## Update the loop (saved in a file) to allows batch resume
        if isbatch
            writestring("$outpath/$batchhash.state", batchcycle+offset-1)
            ## Save network as json (only if it's a batch)
            exportnet_batch(dest, netconf)
        end
        ## Prevent gazebo from running in background when not used
        ## Called that late to allow partial return home
        if hasrobot YARP.gazebo.pause(cron) end
    end # batch
    all_time = toq()
    ## TODO: Can show rt multiplier per batch
    info("All simulation time was $(all_time), that's $(all_time/simn) per simulation")
    ## All successful, delete the state file
    if isbatch rm("$outpath/$batchhash.state") end
end

## inbounds removes array bound checking (should be faster, can't see changes)
## @inbounds
main()
