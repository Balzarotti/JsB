push!(LOAD_PATH, "./libs")

using FORCE
using batch

using Gadfly

## CONFIG

configs = Dict{}("net_size" => 1000:500:2000,
                 "sim_ms" => 3000,
                 "prefix"=>"Batch",
                 "learn_every" => 2,
                 "outdir"=>"./FORCE_example",
                 "dt" => 0.1,
                 "amp" => 1.3,
                 "freq" => 1/60,
                 "dist" => 1.0:0.5:3.0) ## How imprecise is the inverse model
                 ## end config

function B(dist::Float64, z::Float64, prev::Float64)
    return dist *(z - prev)
end

function Xd(step, dt::Float64, amp::Float64, freq::Float64)
    simtime = step * dt
    ((amp/1.0)*sin(1.0*pi*freq*simtime) + (amp/2.0)*sin(2.0*pi*freq*simtime) + (amp/6.0)*sin(3.0*pi*freq*simtime) + (amp/3.0)*sin(4.0*pi*freq*simtime))/1.5
end

function P(m::Float64, prev::Float64)
    return m + prev
end

function main(net::FORCEParams, dt::Float64, amp::Float64, freq::Float64, start::Int64, step_number::Int64, learn::Bool, learn_every::Int64 = 0, dist::Float64 = 1.0)
    m_out = Float64[]
    x_out = Float64[]
    z_out = Float64[]
    w_out = Float64[]

    prev_z::Float64 = net.z

    prev_m = 0.0
    for stepN in start:start+step_number
        stepN::Int64

        x_d = Xd(stepN, dt, amp, freq)

        m = B(dist, net.z + x_d, prev_z)

        x = P(m, prev_m)

        FORCE.step(net,
                   ## Learning
                   learn && mod(stepN, learn_every) == 0,
                   x_d,
                   m,
                   error_override = true,
                   error = x_d - x) ## ? x - x_d? mod?

        mod(stepN, 100) == 0 && println(stepN)
        push!(z_out, net.z)
        push!(m_out, m)
        push!(x_out, x)
        push!(w_out, sqrt(first(net.W' * net.W)))

        prev_z = net.z
        prev_m = m
    end
    (z_out, w_out, m_out, x_out)
end


function save_2(x, basename)
    writecsv("$(basename).csv", x)
    p = plot(y=x, Geom.line)
    draw(SVGJS("$(basename).svg", 30cm, 15cm), p)
end

function save_out(z, w, basename, yfunction)
    ## Save CSV
    writecsv("$(basename)_w.csv", w)
    writecsv("$(basename)_z.csv", z)
    ## Save plots (SVG)
    p = plot(y=w, Geom.line)
    draw(SVGJS("$(basename)_w.svg", 30cm, 15cm), p)
    p1=layer(y=z,Geom.line, Theme(default_color=colorant"red"))
    p2=layer(y=yfunction, Geom.line, Theme(default_color=colorant"green"))
    p = plot(p1,p2)
    draw(SVGJS("$(basename)_z.svg", 30cm, 15cm), p)
end

## Main loop
expanded_configs = batch.recdictcollect(configs)
total = length(expanded_configs)
println("We have to run $(total) simulations. Hold on!")
for (cycle, config) in enumerate(expanded_configs)
    println("Sim: $(cycle) / $(total)")
    const net = FORCEParams(config["net_size"])
    tmp_dict = Dict()
    for (k, v) in config
        if !(k in ["prefix", "outdir"])
            tmp_dict[k] = v
        end
    end
    
    const batch = join(values(tmp_dict), "_")
    println(batch)
    ## feedback 
    tic()
    z_out, w_out, m_out, x_out = main(net,
                                      config["dt"],
                                      config["amp"],
                                      config["freq"],
                                      0,
                                      Int(config["sim_ms"]/config["dt"]),
                                      true,
                                      config["learn_every"],
                                      config["dist"])
    toc()


    mkpath(config["outdir"])
    save_2(x_out, "$(config["outdir"])/$(config["prefix"])_learning_$(batch)_X")
    save_2(m_out, "$(config["outdir"])/$(config["prefix"])_learning_$(batch)_M")

    save_out(z_out, w_out, "$(config["outdir"])/$(config["prefix"])_learning_$(batch)", Xd(0:Int(config["sim_ms"]/config["dt"]),
                                                                                           config["dt"],
                                                                                           config["amp"],
                                                                                           config["freq"]))
    tic()
    ## net::FORCEParams, dt::Float64, amp::Float64, freq::Float64, start::Int64, step_number::Int64, learn::Bool, learn_every::Int64 = 0, dist::Float64 = 1.0)
    z_out_test, w_out_test, m_out_test, x_out_test = main(net,                 ## FORCEParams
                                                          config["dt"],        ## dt::Float64
                                                          config["amp"],       ## amp::Float64
                                                          config["freq"],      ## freq::Float64
                                                          Int(config["sim_ms"] / config["dt"] + 1), # Start::Int64
                                                          Int(config["sim_ms"] / config["dt"]), #Step
                                                          false, #learn
                                                          0, # learn_every
                                                          config["dist"])
    toc()

    save_2(x_out_test, "$(config["outdir"])/$(config["prefix"])_testing_$(batch)_X")
    save_2(m_out_test, "$(config["outdir"])/$(config["prefix"])_testing_$(batch)_M")

    save_out(z_out_test,
             w_out_test,
             "$(config["outdir"])/$(config["prefix"])_testing_$(batch)",
             Xd(config["sim_ms"]/config["dt"]+1:Int(config["sim_ms"]/config["dt"]),config["dt"],config["amp"],config["freq"]))
end
