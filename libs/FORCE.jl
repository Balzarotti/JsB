module FORCE

export FORCEParams ## Object

export step

f(t::Float64;targ_freq::Int64=10) = 1.5sin(2pi * targ_freq*t * 0.001)
    ## Begin and end of the time window dedicated to learning
    ## learn_time_window::Array{Float64}
    ## epsilon::Float64
    ## xsave::Array{Float64}
    ## wsave::Array{Float64}

    ##     M, ## calcolato
    ##     P ## calcolato
    

type FORCEParams
    N::Int ## Network Size
    p::Float64 ## Connection probability
    g::Float64 ## g greater than 1 leads to chaotic networks
    α::Float64 ## "Learning Rate"
    dt::Float64
    M::Array{Float64} ## Weights of inner Reservoir connections (Const)
    nrec2out::Int ## Number of ReadOut neurons (Const)
    W::Array{Float64} ## weigths of Reservoir read out connections
    ## dw::Array{Float64} ## step variation of W 
    WF::Array{Float64} ## Weigth of feedback connections (Const)
    ## Wo::Array{Float64} ## Record of W values in time
    ## zt::Array{Float64} ## Record of z values in time
    ## zpt::Array{Float64}
    r::Array{Float64,1}    
    x::Array{Float64,1}
    ## x0::Array{Float64}
    z::Float64
    P::Array{Float64}
    ## Begin and end of the time window dedicated to learning
    ## learn_time_window::Array{Float64}
    ## epsilon::Float64
    ## xsave::Array{Float64}
    ## wsave::Array{Float64}

    ##     M, ## calcolato
    ##     P ## calcolato
    
    function FORCEParams(N::Int # Network Size, given by JsB
                         , p::Float64
                         , g::Float64
                         , α::Float64 ## "Learning Rate"
                         , dt::Float64
                         , nrec2out
                         , W
                         , WF
                         , x
                         )
        ## ## Calulated
        ## scale = 1.0/sqrt(p*N)
        M = sprandn(N,N,p)*g*1.0/sqrt(p*N)
        M = full(M)

        ## W = zeros(nRec2Out,1)
        ## dw = zeros(nRec2Out,1)
        ## WF = 2.0*(rand(N,1)-0.5)

        # wo_len = zeros(1,simtime_len)    
        # zt = zeros(1,simtime_len)
        # zpt = zeros(1,simtime_len)
        ## z0 = 0.5*randn(1,1)

        r = tanh(x)

        z = W'*r
        
        P = (1.0/α)*eye(nrec2out)

        new(N, ## imposto
            p, ## imposto
            g, ## imposto
            α, ## imposto
            dt, ## imposto
            M, ## calcolato
            nrec2out, ## imposto
            W, ## imposto
            WF, ## imposto
            r, ## calcolato
            x, ## imposto
            z[], ## calcolato
            P ## calcolato
            )
    end
end # type

"Create a FORCE network with standard (Sussillo, 2009) values"
function FORCEParams(N::Int)
    p = 0.1 # p::Float64
    g = 1.5# g::Float64
    α = 1.0 #α::Float64 ## "Learning Rate"
    dt = 0.1 #dt::Float64
    nrec2out = N
    W = zeros(nrec2out)
    WF = 2.0*(rand(N)-0.5)
    x = 0.5 * randn(N)
    
    FORCEParams(N::Int # Network Size, given by JsB
                , p::Float64
                , g::Float64
                , α::Float64 ## "Learning Rate"
                , dt::Float64
                , nrec2out
                , W
                , WF
                , x)
end

## feedback 
function z_transf(z::Float64)
    return 1.1*tanh(sin(z))
end

function step(params::FORCEParams, learning::Bool, desired_output::Float64, feedback::Float64; error_override::Bool = false, error::Float64 = 0.0)
    params.x = (1.0-params.dt)*params.x + params.M *
    params.r*params.dt + params.WF*feedback*params.dt

    params.r = tanh(params.x)

    params.z = dot(params.W,params.r)
    
    k::Array{Float64,1}
    ## rPr::Float64
    c::Float64
    e::Float64
   
    if learning
        ## update inverse correlation matrix
        k = params.P*params.r
        # rPr = dot(params.r,k)
        c = inv(1.0 + dot(params.r,k))
        params.P -= k*(k.*c)'
        
        ## update the error for the linear readout
        if error_override
            e = first(params.z - desired_output)
        else
            e = error
        end

        ## update the output weights
        ## W = W + dw
        params.W -= e.*c.*k
    end

    nothing
    ## ## Store the output of the system.
    ## zo.append(z)
    ## Wo.append(sqrt(W'*W))
end

end # module
