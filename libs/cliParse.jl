module cliParse

import ArgParse

## Defaults allow compatibility with shorter cli.json save-file (forward compatibility)
cli_defaults = Dict("steps" => 30000,
                    "output" => "history",
                    "output-file" => "",
                    "no-history-update" => false,
                    "save-membrane" => false,
                    "save-membrane-steps" => 1,
                    "no-save-membrane" => false,
                    "show-image-spikes" => false,
                    "save-image-spikes" => false,
                    "show-image-membranes" => false,
                    "save-image-membranes" => false,
                    "save-config" => "", 
                    "override-config" => false,
                    "export-network" => false,
                    "export-network-override" => false,
                    "run" => false,
                    "load-config" => "",
                    "yarp-server" => "127.0.0.1",
                    "disable-robot" => false,
                    "yarp-port" => 10000,
                    "yarp-clock" => "/clock/rpc",
                    "robot-name" => "/singlePendulumGazebo/body/rpc:i",
                    "robot-writer-name" => "/julia/robotctrl",
                    "robot-velocity" => 10,
                    "clock-writer-name" => "/julia/clockctrl",
                    "no-save-angles" => false,
                    "network" => "./nets/testnet.jl",
                    "disable-batch-resume" => false,
                    "new-dir-if-exists" => false
                    )

export cli_defaults
export parse_commandline
function parse_commandline()
    s = ArgParse.ArgParseSettings()

    @ArgParse.add_arg_table s begin
        "--steps", "-s"
        help = "Number of steps to simulate (ms)"
        default = 30000
        arg_type = Int
        "--output", "-p"
        help = "Output path"
        default = cli_defaults["output"]
        "--output-file", "-o"
        help = "Output file name. Disables sha1 based namings"
        default = cli_defaults["output-file"]
        "--no-history-update", "-P" ## Stands for Private
        help = "Don't add simulation to the history.log (placed in output path)."
        action = :store_true
        ## TODO? add other crypto (sha1 is already the fastest?)
        "--save-membrane"
        help = "Save membrane of all neurons (slow)"
        action = :store_true
        "--save-membrane-steps"
        help = "Save membrane frequency" ## TODO: add custom time type and convert (ie ms - s - m - h)"
        default = cli_defaults["save-membrane-steps"]
        arg_type = Int
        "--no-save-membrane"
        help = "Disable saving membrane (overrides netconf)"
        action = :store_true
        "--show-image-spikes"
        help = "Show the rasterplot of spikes"
        action = :store_true
        "--save-image-spikes"
        help = "Save the rasterplot of spikes"
        action = :store_true
        "--show-image-membranes"
        help = "Show the membrane potential"
        action = :store_true
        "--save-image-membranes"
        help = "Save the membrane potential"
        action = :store_true
        "--save-config"
        help = "Saves current command line to file [filename].json"
        default = cli_defaults["save-config"]
        "--override-config"
        help = "Override config file, if already exists (without confirmation"
        action = :store_true
        "--export-network"
        help = "After parsing the network config, export it as json"
        action = :store_true
        "--export-network-override"
        help = "Overrides [network-name].json file, if exporting network, if exits"
        action = :store_true
        "--run", "-r"
        help = "Run the simulation, after saving it (used only with --save-config)"
        action = :store_true
        "--load-config"
        help = "Load config from [filename].json as it was passed to CLI"
        default = cli_defaults["load-config"]
        "--yarp-server"
        help = "IP of the YARP server. NULL has the same effect of --disable-robot "
        default = cli_defaults["yarp-server"]
        "--disable-robot"
        help = "Disable all the YARP/gazebo/jSpike part"
        action = :store_true
        "--yarp-port"
        help = "Port of the YARP server"
        default = cli_defaults["yarp-port"]
        "--yarp-clock"
        help = "Path of gazebo-yarp-plugin clock"
        default = cli_defaults["yarp-clock"]
        "--robot-name"
        help = "YARP robot to control"
        default =  cli_defaults["robot-name"]
        "--robot-writer-name"
        help = "Name of the writer port used by this program"
        default = cli_defaults["robot-writer-name"]
        "--robot-velocity"
        help = "Define velocity of joints (same value for all)"
        default = cli_defaults["robot-velocity"]
        "--clock-writer-name"
        help = "Name of the clock writer port used by this program"
        default = cli_defaults["clock-writer-name"]
        "--no-save-angles"
        help = "Even if robot is present, don't save angles to file
If yarp reader is present, they are still read, just not saved"
        action = :store_true
        "--disable-batch-resume"
        help = "Do not recover interrupted batch, even if the state file exists.\nThe state file will be overritten at first successful run"
        action = :store_true
        "--new-dir-if-exists"
        help = "Append a counter like _00 to the end of the output dir. This allows multiple batch, same config, but in multiple folders"
    action = :store_true
        "network"
        help = "The network file to use"
        default = cli_defaults["network"]
    end

    return ArgParse.parse_args(s)
end
end ## module
