module batch

using Iterators

## Thanks to http://stackoverflow.com/questions/37433071/generate-all-combination-of-dictionaries-containing-ranges
function findranges{K}(sd::Dict{K})
    ranges = Vector{Vector}()
    for v in values(sd)
        if isa(v,Range)
            push!(ranges,collect(v))
        elseif isa(v,Dict)
            push!(ranges,recdictcollect(v))
        elseif isa(v,Vector)
            push!(ranges,
                  map(x->vcat(x...),
                      collect(product(map(recdictcollect,v)...))))
        end
    end
    ranges
end

recdictcollect(sd::Int) = sd

function recdictcollect{K}(sd::Dict{K})
    ranges = findranges(sd)
    if length(ranges)==0
        cases = [()]
    else
        cases = product(ranges...) |> collect
    end
    outv = Vector{Dict{K,Any}}()
    for c in cases
        newd = Dict{K,Any}()
        i = 1
        for (k,v) in sd
            if any([isa(v,t) for t in [Range,Dict,Vector]])
                newd[k] = c[i]
                i += 1
            else
                newd[k] = v
            end
        end
        push!(outv,newd)
    end
    return outv
end

end ## module batch
