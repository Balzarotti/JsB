"Spikes-to-angle and angles-to-spikes conversion"
module jSpike

import Distributions

type NeuronGroupIn
    nemoids::Vector{Int}
    std::Float64
    min::Float64
    max::Float64
    peak::Float64
    costantI::Float64
    Iinc::Float64
    ## width::Int
    ## eight::Int
    angles::Vector{Float64}
    anglesd::Float64
    normaldist::Distributions.Distribution
    currentfactor::Float64

    function NeuronGroupIn(nemoids::Vector{Int},
                           std::Float64,
                           min::Float64,
                           max::Float64,
                           peak::Float64,
                           constantI::Float64,
                           Iinc::Float64)
        anglesd = std * (max - min)/(length(nemoids) - 1) ## = * angledist
        normaldist = Distributions.Normal(0, anglesd)
        currentfactor = peak / Distributions.pdf(normaldist, 0)

        new(nemoids,
            std,
            min,
            max,
            peak,
            constantI,
            Iinc,
            anglesspace(min, max, length(nemoids)), ## angles
            anglesd,
            normaldist,
            currentfactor) ## last saved angle
    end
end

type NeuronGroupOut
    nemoids::Vector{Int}
    spikeids::Vector{Int} ## Automatically created [1:length(neuronids)]
    angles::Vector{Float64} ## Auto created linspace(min, max, length(neuronids))
    min::Float64
    max::Float64
    currentInc::Float64
    decayrate::Float64
    ## integrationsteps::Int
    ## width::Float64
    ## height::Float64
    ## dof::Int

    ## TODO: state variables
    currentvars::Vector{Float64}
    ## currentangle::Float64
    ## Functions: setfirings
    
    function NeuronGroupOut(neuronids::Vector{Int},
                            min::Float64,
                            max::Float64,
                            currentInc::Float64,
                            decayrate::Float64)
        if length(neuronids) < 2
            error("Neuron array too small")
        elseif min > max
            error("Min angle should be more than max!")
        else
            if  ! (0 < decayrate < 1)
                warn("Decay rate should be between 0 and 1!")
            end
            new(neuronids,
                collect(1:length(neuronids)), ## Spikesids
                anglesspace(min, max, neuronids), ## Angles
                min,
                max,
                currentInc,
                decayrate,
                ## integrationsteps,
                zeros(length(neuronids))
                ## 0)
                )
        end
    end
end

anglesspace(min, max, neuronids::Vector) = anglesspace(min, max, length(neuronids))
anglesspace(min, max, length) =  collect(linspace(min, max, length))

## Define functions
setfirings(group, firings::Vector{Bool}) = group.currentvars += firings*group.currentInc

function step(group) ## Out
    group.currentvars *= group.decayrate
    anglesum::Float64 = 0
    weightsum::Float64 = 0
    newangle::Float64 = 0.0
    
    anglesum = sum(group.angles .* group.currentvars)
    weightsum = sum(group.currentvars)
    if weightsum != 0
        newangle = anglesum/weightsum
    end
    ## Clamp to max/min and return
    clamp(newangle, group.min, group.max)
end

## step(group, newangle) = step(group, convert(Float64, newangle))
function step(group, newangle::Float64) ## GROUP IN
    ## Clamp to max/min
    newangle = clamp(newangle, group.min, group.max)
    ## println(group.angles - newangle)
    group.costantI+group.currentfactor*Distributions.pdf(group.normaldist, group.angles - newangle)
    ## outputcurrent = group.constantinc + group.Iinc * pdf
end

end ## module
               
