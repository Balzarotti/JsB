"Define various conversion methods, analysis etc" #TODO: move analysis
module spikesIO

using Gadfly
set_default_plot_size(30cm, 15cm)

import JSON
import GZip
import SHA
import batch
export t_str
export load_cli_conf, loadconf, save_config, stimuli, cleancli
export run_or_exit, getname, updatehistory
export loadnets, saveimages, exportnet_batch, exportnet_and_die
export getoutputpath, checkbatch_loop, save_membrane
export dict2json, savejson
export readint, writeoutput, historyappend, writestring
export checkrobot, checkjointnumber

macro getdefault(item)
    ## Return the cli argument if exists, cli_defaults else
    return :( get(cli, $item, cli_defaults[$item]) )
end

"""
    `t"time"`

Introduce a new string type: time. Converts time units in milliseconds Float64
ie. `t"3ms" = 1000.0`
"""
macro t_str(time) ## If none specified, assume ms
    ## Determine time unit
    second = 1000
    minute = 60 * second
    hour = 60 * minute
    day = 24 * hour
    year = 365 * day
    re = r"(^[0-9\.]+)"
    if ismatch(r"^[0-9\.]+y$", time) ## (y)ears
        parse(Float64, match(re,time)[1]) * year
    elseif ismatch(r"^[0-9\.]+d$", time) ## (d)ays
        parse(Float64, match(re,time)[1]) * day        
    elseif ismatch(r"^[0-9\.]+h$", time) ## (h)ours
        parse(Float64, match(re,time)[1]) * hour
    elseif ismatch(r"^[0-9\.]+m$", time) ## (m)inutes
        parse(Float64, match(re,time)[1]) * minute
    elseif ismatch(r"^[0-9\.]+[^m]s$", time) ## (s)econds
        parse(Float64, match(re,time)[1]) * second
        ## (m)illi(s)econds
    elseif ismatch(r"^[0-9\.]+ms$", time) || ismatch(r"^[0-9\.]+$", time)
        parse(Float64, match(re,time)[1])
    else
        error("Malformed time unit literal t\"$time\"")
    end
end

function checkbatch_loop(cli, cli_defaults, simn, outpath, netconfs)
    isbatch::Bool = simn > 1
    offset::Int
    if isbatch
        ## Used for unique save-state name
        batchhash = SHA.sha1(dict2json(netconfs))
        info("This config has $simn simulations")
        if simn >= 10000
            warn("That's a big number")
        end
        if isfile("$outpath/$batchhash.state") && ! @getdefault "disable-batch-resume"
            info("Resume state for this session exists")
            offset = readint("$outpath/$batchhash.state")
        else
            offset = 1
        end
    else
        offset = 1
        batchhash = ""
        info("Running a single simulation")
    end
    isbatch, offset, batchhash
end

function save_membrane(cli, cli_defaults, netconf)
    membraneidxs::Vector{UInt32} = []
    if get(cli, "save-membrane", cli_defaults["save-membrane"]) ## CHECKME
        ## membraneout::Array{Float32}
        if get(cli, "no-save-membrane", cli_defaults["no-save-membrane"])
            error("You are asking to both save and don't save membrane (CLI)!")
        else
            const savemembrane = true
            membraneidxs = collect(1:n)
            savemembsteps = get(cli, "save-membrane-steps", cli_defaults["save-membrane-steps"])
        end
    elseif get(cli, "no-save-membrane", cli_defaults["no-save-membrane"])
        const savemembrane = false
    else
        if haskey(netconf, "save-membrane")
            const savemembrane = true
            membraneidxs = netconf["save-membrane"]["neurons"]
            savemembsteps = get(netconf["save-membrane"], "every", 1)
            tmp = get(cli, "save-membrane-steps", cli_defaults["save-membrane-steps"])
            if savemembsteps != tmp ## TODO: why save it in config?
                warn("Your config and your CLI save membrane steps differs! Using CLI ($tmp ms)")
                savemembsteps = tmp
            end
        else
            const savemembrane = false
        end
    end
    savemembrane, isdefined(:savemembsteps) ? savemembsteps : 1, membraneidxs
end

function updatehistory(cli, cli_defaults, filename, outpath, elapsed, steps, n)
    if ! get(cli, "no-history-update", cli_defaults["no-history-update"])
        ## TODO: Can be added however if needed
        if get(cli, "output-file", cli_defaults["output-file"]) != ""
            warn("Not updating history (?useless with output-file defined)!")
        else ## omething else? Is it useful?
            historyappend("$outpath/history.log",
                          hcat(split(filename,"_")[1],
                               split(filename,"_")[2],
                               elapsed,
                               steps,
                               n))
        end
    end
end

function getname(cli, cli_defaults, netconf)
    hashin = cleancli(cli)
    outfile = @getdefault "output-file"
    if outfile == ""
        ## FIXME: needs to use the fixed netconf when batch enabled
        networkhash = SHA.sha1(dict2json(netconf))
        confighash = SHA.sha1(dict2json(hashin))
        confighash*"_"*networkhash # return
    else
        outfile
    end
end

"""
    getoutputpath()

Return the path of the output, defined by config, and checking 
"""
function getoutputpath(cli, cli_defaults)
    outpath = @getdefault "output"
    if @getdefault "new-dir-if-exists"
        ## FIXME: Hopefully never more then 999?
        for loop in 1:999
            newpath = "$outpath" * @sprintf "_%03.i" loop
            if isdir(newpath)
                continue
            else
                break
            end
        end
        outpath = newpath
    end
    outpath
end

"""
    getisi(spikes::Vector{Bool})

Returns a same-length array of the ISI (interspike interval)
"""
function getisi(spikes::Vector{Bool})
    ## using DataFrames
    totalms = length(spikes)
    ## Start timing from 1 or it's shifted
    arr = spikes[1] != 1 ? [1] : []
    ## Find changes. It's a bool vector :)
    append!(arr, find(spikes))
    ## Find the distances
    isi = diff(arr)
    isims = []
    for i in isi
        append!(isims, [ i for _ in 1:i ])
    end
    for i in 1:totalms - arr[end] + 1
        push!(isims, isi[end])
    end
    isims
end

function save_config(cli, cli_defaults)
    savefile = @getdefault "save-config"
    if savefile != ""
        info("Saving config to $savefile")
        saveconf(cli)
        true
    else
        false
    end
end

function run_or_exit(cli, cli_defaults)
    ## Check if we need to exit
    if (! @getdefault "run") && (! @getdefault "export-network")
        warn("--run (or -r) not given, exiting")
        exit()
    end
end

## function initrobot(cli, cli_defaults)
##         robotname = @getdefault "robot-name"
##         clockname = @getdefault "yarp-clock"

##         writername = @getdefault "robot-writer-name"
##         clockctrl = @getdefault "clock-writer-name"

##         net = YARP.newnet()
##         robot = gazebo.connect(net, writername, robotname)
##         cron = gazebo.connect(net, clockctrl, clockname)

##         joints = gazebo.getaxes(robot)
##         info("$robotname has $joints axes")

##         ## TODO: Add reader
##         haswriter = get(netconf["sensory"], "writer", "") != ""
##         if haswriter
##             ## Prepare variables
##             outputangle::Float64 = 0.0
##             neuronindex::Int = 0
##             outwriter::Array{Float64} = zeros(joints, steps)
##             ## Create output networks and populate a outs
##             outs = Dict()
##             for grp in netconf["sensory"]["writer"] ## TODO: check for duplicates
##                 if ! checkjointnumber(joints, grp)
##                     continue
##                 end
##                 outs[grp["joint"]] = jSpike.NeuronGroupOut(collect(grp["neurons"]),
##                                                            float(grp["min"]),
##                                                            float(grp["max"]),
##                                                            float(grp["increment"]),
##                                                            float(grp["decay"]))
##             end
##         end
##     end
## end

function load_cli_conf(cli, cli_defaults)
    ## Do we need to load a config file?
    if get(cli, "load-config", cli_defaults["load-config"]) != ""
        ## Check that the file we want to load really exists
        if isfile(get(cli, "load-config", cli_defaults["load-config"]))
            info("Using config file $(get(cli, "load-config", cli_defaults["load-config"]))")
            cli = loadconf(get(cli, "load-config", cli_defaults["load-config"]))
            println(cli) ## Let user know what is using
            ## FIXME: merge: how to do this?
            warn("Your CLI params are IGNORED (overridden by the conf)!")
        else error("File $(get(cli, "load-config", cli_defaults["load-config"])) does not exists!")
        end
    end
    ## Returns the cli (does not matter if it changed or not)
    cli
end

function exportnet_and_die(cli, cli_defaults, netconf)
    if @getdefault  "export-network"
        ## It's named like the input file
        expname = match(r"(.*).jl", @getdefault "network")[1]
        info("Saving network to $expname")
        if ! isfile(expname) || @getdefault "export-network-override"
            savejson("$expname.json", netconf)
        else
            warn("Cannot save network. File exists. Use --export-network-override")
        end
        if !@getdefault "run"
            warn("--run (or -r) not given, exiting")
            exit()
        end
    end
end

function exportnet_batch(dest, netconf)
    expname = "$(dest)_config.json"
    savejson("$expname", netconf)
end

function saveimages(cli, cli_defaults, outspikes, dest, steps)
    show = @getdefault "show-image-spikes"
    save = @getdefault "save-image-spikes"
    savename = save ? "" : "$dest.spikes"
    if save || show
        spikesplot(outspikes, 1:steps,
                   show = show,
                   savename = savename)
    end
end                   

lastsim() = lastsim("history/history.log")

lastsim(historypath) = open(historypath, "r") do fh
    split(strip(readlines(fh)[end], '\n'), ",")
end

function nextsim(path)
    open(path, "r") do fh
        for ln in eachline(fh)
            l = split(strip(ln, '\n'), ",")
            produce(l[1:2],parse(Float64,l[3]),parse(Int,l[4]),parse(Int,l[5]))
        end
    end
end

## IPv6: http://stackoverflow.com/questions/53497/regular-expression-that-matches-valid-ipv6-addresses
"Regex ip matching (both IPv4 and IPv6)"
checkip(ip::AbstractString) = ismatch(r"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$", ip) || ismatch(r"(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))", ip)

"Check cli and network for robot (takes care of various disable ability )"
function checkrobot(cli, cli_defaults, net) ##FIXME: write more legible
    if @getdefault "disable-robot"
        return false
    else
        ## Check if yarp-server is a valid ip
        if ! checkip(cli["yarp-server"])
            if cli["yarp-server"] == "NULL"
                ## NULL can be used to disable robot
                return false
            else
                warn("YARP IP address $(cli["yarp-server"]) might be wrong")
            end
        end
        ## Has sensory defined in net? Else robot not needed
        if get(net, "sensory", "") != ""
            ## Has either reader or writer?
            if get(net["sensory"], "reader", "") != "" || get(net["sensory"], "writer", "") != ""
                return true
            else
                return false
            end
        else
            return false
        end
    end
end

checkjointnumber(joints, grp) = if joints < grp["joint"]
    warn("Your network config has more joint then your robot!")
    warn("Not adding joint: $(grp["joint"])")
    false
else true
end


function cleancli!(ncli::Dict, keep::Vector{ASCIIString})
    for (k, v) in ncli
        ! (k in keep) ? delete!(ncli, k) : ""
    end
end
function cleancli(cli::Dict) ## Saves only "steps" now O.o
    ## Will save things like pid, sync, neuron network override etc
    const keep::Vector{ASCIIString} = [ "steps" ]
    ncli = copy(cli)
    ## cleancli!(ncli, keep)
    ncli
end

function historyappend(filename::AbstractString, line)
    open(filename, "a") do x
        writecsv(x, line)
    end
end

"""
    loadnets()

Return a Vector{Dict} of all the network config in a file (be it a batch or not)
"""
function loadnets(cli, cli_defaults)
    network_file_loaded = loadnet(@getdefault "network")
    ## netconfs: Vector of Dict, containing all the sims we need to run
    batch.recdictcollect(network_file_loaded)
end

## "Load a network .json file"
## loadnet(netfile) = JSON.parsefile(netfile)
"Load a .jl network file"
function loadnet(netfile)
    if ismatch(r".*\.jl$", netfile)
        ## julia file (.jl)
        include(netfile)
    else ## json file (.json)
        JSON.parsefile(netfile)
    end
end

loadconf(conffile) = JSON.parsefile(conffile)

function writestring(output, int)
    open(output, "w") do x
        write(x, string(int))
    end
end

function readstring(input)
    open(input, "r") do x
        readall(x)
    end
end

function readint(input)
    parse(Int, readstring(input))
end

"Private"
dict2json(data) = JSON.json(data)
function savejson(filename, data; echo = false)
    fh = open(filename, "w")
    dataw = JSON.json(data)
    write(fh, dataw)
    close(fh) ## TODO: add success/failure + return
    if echo ## TODO: add opion to remove empty param from cli
        ##                (replace cli[""] with get() in main)
        info("This is the config that has been saved:")
        for (k, v) in data
            println("$k => $v")
        end
    end
end
#::AbstractString,
macro ask(askmode, text)
    return :(
             begin
                 $askmode($text)
                 "y" == strip(readline(STDIN))
             end
             )
end

"Save CLI to file. Remove params as \"--save-config\""
function saveconf(cli)
    ## TODO: various fixes if needed
    ## Get output file, clean it
    outfile = @getdefault "save-config"
    cli["save-config"] = "" ## Prevents endless save loops XS
    save = if ! @getdefault "override-config" && isfile("$outfile")
        @ask warn "File $outfile exists, override? [N/y]"
    else
        true
    end
    if save
        mkpath(dirname(outfile))
        savejson("$outfile", cli; echo = true)
        info("Config saved to $outfile")
    else info("Config NOT saved!") end
end

"Find the key corresponding to the maximum value in a dict"
function dictmax(dict)
    modesArray = Dict() # Array of the modes so far
    max = 0 # Max of repetitions so far
    # Update modesArray if the number of repetitions
    # of v reaches or surpasses the max value
    for (i, v) in dict
        if v >= max
            if v > max
                empty!(modesArray)
                max += 1
            end
        append!(modesArray, [v])
        end
        modesArray
    end
end

function modeshist(values)
    dict = Dict() # Values => Number of repetitions
    for v in values
        # Add one to the dict[v] entry (create one if none)
        if v in keys(dict)
            dict[v] += 1
        else
            dict[v] = 1
        end
    end 
    dict
end

searchdir(path,key) = filter(x->contains(x,key), readdir(path))
#listoutputs(path) = searchdir(path, "_output") ## TODO: fix for new type
hashes_from_names(filename) = split(filename, '_')[1:2]
## Not used really
readbzip(filepath) = run(`bzcat $(filepath)` |> STDOUT)
inputpath(path, config_hash) = joinpath(path, config_hash) * "_input.py"
#readinput(filepath) = imp.load_source("*", filepath)

## function readgzip(filepath)
##     info("Opening " * filepath)
##     fh = GZip.open(filepath)
##     lines = readlines(fh)
##     close(fh)
##     lines
## end

#function readoutput(filepath)
#    if ismatch(r".gz$", filepath)
#        data = readgzip(filepath)[1]
#    else
#        error("Output format not recognized!")
#    end
#    ast.literal_eval(data)
#end

function spikelisttoarray(spikes, siminput)
    ## "+1" fixes python numbering
    sequence::Array{Bool} ## Faster 1.3x then bitArray
    sequence = falses(length(spikes)+1,maximum(siminput[:save])+1)
    for (ms, neurons) in enumerate(spikes)
        for nid in neurons
            sequence[ms+1,nid+1] = true
        end
    end
    sequence
end

function runningmean(serie, window)
    dim = length(serie)
    output = zeros(dim)
    ## Can optimize it
    for frame in 1:dim
        d = frame - window
        firstf = d > 1 ? d : 1
        output[frame] = mean(serie[firstf:frame])
    end
    output
end

#FIXME: add split ranges
spikestosquare(spikes;
               window=15,
               threshold=.05) = runningmean(spikes,
                                            time_window) .> threshold

mode(mdic::Dict) = collect(keys(mdic))[indmax(collect(values(mdic)))]

function getfreqs(serie) #Can do better right?
    states = Dict(0 => [], 1 => [])
    laststate = serie[1]
    lastchange = 1
    for (ms, frame) in enumerate(serie)
        if frame != laststate
            push!(states[laststate], ms - lastchange)
            laststate = frame
            lastchange = ms
        end
    end
    push!(states[laststate], length(serie) + 1 - lastchange)
    states
end

"Find the maximum value for the index-th column of a dict
TODO: replace with macro"
function dictvalmax{T <: Union{AbstractString,Int}}(dict::Dict, index::T)
    maxv = 1
    for vals in values(dict)
        for val in vals
            maxv = max(maxv, val[index])
        end
    end
    maxv
end

function dictvalmin{T <: Union{AbstractString,Int}}(dict::Dict, index::T)
    minv = 1
    for vals in values(dict)
        for val in vals
            minv = min(minv, val[index])
        end
    end
    minv
end

"Return dict wiithout element which owns \"disabled\": true"
function rmdisabled(dict)
    out = Dict() ## How to do this without copying the dict?
    for (k, vals) in dict
        for (i, v) in enumerate(vals)
            !haskey(v, "disabled") || v["disabled"] == false ? out[k] = vals : ""
        end
    end
    out
end

"Creates the array of stimuli. Returns a tuple (matrix, startms, stopms)
Float32 NxM matrix (N = neurons, M = max_ms - min_ms)"
function stimuli(stimmap::Dict, neurons::Real)
    stimmap = rmdisabled(stimmap)
    minms = Int(dictvalmin(stimmap, "start")) ## Minimum between starts
    minms == 0 ? error("First ms of simulation is 1, not 0! Fix your stimuli"):""
    maxms = Int(dictvalmax(stimmap, "stop")) ## Maximum between ends
    stimarr = zeros(Float32, neurons, maxms - minms + 1)
    idxdict = Dict{Int,Vector{Int}}()
    Idict = Dict{Int,Vector{Float32}}()
    for (n, stimuli) in stimmap ## Iterate every neuron
        nid = typeof(n) != Int ? nid = parse(Int, n) : n
        nid <= 0 ? error("Not allowed neuron id = 0 in stimuli!") : ""
        nid > neurons ? error("A stimulus is given to a non-existing neuron!") : "" ## Could be a warn?
        for s in stimuli ## Iterate every stimulus for the current neuron
            stimtype = get(s, "type", "constant")
            for i in s["start"]:s["stop"] ## TODO: set defaults (ie gain)
                ## and WARN if default used
                ## Prepares a lot of data, but hopefully speeds up runtime
                valtoadd = begin
                    if stimtype == "constant"
                        s["current"]
                    elseif ismatch(r"^sin", stimtype)
                        sinstim(s, i)
                    elseif ismatch(r"^cos", stimtype)
                        cosstim(s, i)
                    elseif stimtype == "square"
                        error("Not implemented yet") ## Duty cycle etc
                        squarestim(s, i)
                    end
                end
                stimarr[nid, (i - minms + 1)] = valtoadd
                ## haskey(Idict, i) ? push!(Idict[i],valtoadd) : Idict[i] = [valtoadd]
                haskey(idxdict, i) ? push!(idxdict[i],nid) : idxdict[i] = [nid]
            end
            for (k, v) in idxdict
                idxdict[k] = sort(v)
            end
        end
    end
    #! WARNING: idxarr starts at ms - minms (always starts at 1, you need to
    # adjust the loop (ms) number accordingly.
    # stimarr, instead, is a dict with the RIGHT ms value
    #    (idxdict, stimarr, minms, maxms)
    (idxdict, stimarr, minms, maxms)
end

## MACRO!
## FIXME: start at minumum? or 0 (like now)? Or max?
sinstim(stim, i) = (sin(2pi*stim["freq"]*(i - stim["start"] + get(stim, "ms-shift", 0))/1000)*stim["gain"]) + stim["offset"]
cosstim(stim, i) = (cos(2pi*stim["freq"]*(i - stim["start"] + get(stim, "ms-shift", 0))/1000)*stim["gain"]) + stim["offset"]
## squarestim(stim, i) = sinestim(stim, i) > 0 ## TODO


## "Creates the array of stimuli. Returns a tuple (matrix, startms, stopms)
## Float32 NxM matrix (N = neurons, M = max_ms - min_ms)"
## function stimuli(stimmap::Dict, neurons::Real)
##     stimmap = rmdisabled(stimmap)
##     ## TODO: add the array value "disabled": true/false to bypass a stimulus
##     minms = Int(dictvalmin(stimmap, "start")) ## Minimum between starts
##     minms == 0 ? error("First ms of simulation is 1, not 0! Fix your stimuli"):""
##     maxms = Int(dictvalmax(stimmap, "stop")) ## Maximum between ends
##     stimarr = zeros(Float32, neurons, maxms - minms + 1)
##     idxdict = Dict{Int,Vector{Int}}()
##     idxarr = falses(neurons, maxms - minms + 1)
##     for (n, stimuli) in stimmap
##         nid = typeof(n) != Int ? nid = parse(Int, n) : n
##         nid <= 0 ? error("Not allowed neuron id = 0 in stimuli!") : ""
##         nid > neurons ? error("A stimulus is given to a non-existing neuron!") : "" ## Could be a warn?
##         for s in stimuli
##             for i in s["start"]:s["stop"]
##                 stimarr[nid, (i - minms + 1)] = s["current"]
##                 idxarr[nid, (i - minms + 1)] = true
##                 haskey(idxdict, i) ? push!(idxdict[i],nid) : idxdict[i] = [nid]
##             end
##         end
##     end
##     #! WARNING: idxarr starts at ms - minms (always starts at 1, you need to
##     # adjust the loop (ms) number accordingly.
##     # stimarr, instead, is a dict with the RIGHT ms value
##     (idxdict, stimarr, minms, maxms)
## end

## Macro?
"True if file is a compressed gz"
isgz(name) = ismatch(r".gz$", name)
israw(name) = ismatch(r".raw$", name)

"Save to file all the data given sequentially"
function writebinfile(filename::AbstractString, data::Tuple)
    fh = isgz(filename) ? GZip.open(filename, "w") : open(filename, "w+")
    for raw in data
        write(fh, raw)
        ## flush(fh)
    end
    close(fh)
end

"Read UInt32 from stream"
readfirsthead(stream) = read(stream, UInt32)

## TODO: Unify those things a bit, providing helpers
"Read spikes from raw/raw.gz. 32bit+32bit header array size"
function readbinfile(filename::AbstractString)
    fh = isgz(filename) ? GZip.open(filename, "r") : open(filename, "r")
    n = readfirsthead(fh)
    steps = readfirsthead(fh)
    info("This file has $n neurons, $steps steps")
    ret = read(fh, Bool, (convert(Int64, n), convert(Int64, steps)))
    close(fh)
    ret
end

function readbinfile(fh::IO)
    n = readfirsthead(fh)
    steps = readfirsthead(fh)
    info("This file has $n neurons, $steps steps")
    read(fh, Bool, (convert(Int64, n), convert(Int64, steps)))
end

function readfloatfile(fh::IO, filetype::AbstractString)
    n = Int(readfirsthead(fh))
    ssteps = Int(readfirsthead(fh))
    extra = Vector{Int}
    tmp = 0
    if filetype == "stimuli"
        ## extra = (minstimms, maxstimms)
        extra = [Int(readfirsthead(fh)), Int(readfirsthead(fh))]
        tmp = ssteps
        ssteps = extra[2] - extra[1] + 1
        extra[2] = tmp
        ## extra is now (minstimms, steps). ssteps is the length
        info("Found $n neurons, $(extra[2]) steps, starting at $(extra[1]), stopping at $ssteps ms")
    elseif filetype == "membranes"
        steps = Int(readfirsthead(fh))
        samplerate = Int(steps / ssteps)
        info("Found $n neurons, $ssteps steps, sampled every $samplerate ms")
        extra = [steps]
    elseif filetype in ["angles", "writer"]
        extra = [0]
        info("Found $n joints, $ssteps steps")
    else
        error("Unknown file type")
    end
    ret = read(fh, Float64, (convert(Int64, n), convert(Int64, ssteps)))
    close(fh)
    extra, ret
end

function readfloatfile(filename::AbstractString)
    fh = isgz(filename) ? GZip.open(filename, "r") : open(filename, "r")
    n = readfirsthead(fh)
    membsteps = Int(readfirsthead(fh))
    steps = Int(readfirsthead(fh))
    samplerate = Int(steps / membsteps)
    info("This file has $n neurons, $steps steps, sampled every $samplerate ms")
    ret = read(fh, Float64, (convert(Int64, n), convert(Int64, membsteps)))
    close(fh)
    steps, ret
end

const HEADERTABLE = Dict("spikes" => 0x2acf69c6, "membranes" => 0x2acf69c7, "stimuli" => 0x2acf69c8, "angles" => 0x2acf69c9, "writer" => 0x2acf69ca) ## Just incremental from 718236102
const REVERSEHEADER = Dict{UInt32, AbstractString}([ v => k for (k, v) in HEADERTABLE])

"This function detect output type from headers and read it accordingly.
See HEADERTABLE. Headers are saved as UInt32."
function readoutput(filename::AbstractString)
    fh = isgz(filename) ? GZip.open(filename, "r") : open(filename, "r")
    filetype = get(REVERSEHEADER, readfirsthead(fh), "unknown")
    info("File type is $filetype")
    if filetype == "spikes"
        input = readbinfile(fh)
    elseif filetype in ["membranes", "stimuli", "angles", "writer"]
        input = readfloatfile(fh, filetype)
    else
        error("Unknown input file type!?")
    end
    close(fh)
    input
end

"Write output file as binary data. Auto detect type (spikes/membrane/stimuli) from name, or provide it with \"filetype\" arg. Auto detect compression type"
function writeoutput(filename::AbstractString, data; filetype = "auto")
    if filetype == "auto"
        regex = r"^.*\.([a-zA-Z]+)\."
        regexnotstrict = r"^.*\.([a-zA-Z]+)"
        if ismatch(regex, filename)
            filetype = match(regex, filename)[1]
        elseif ismatch(regexnotstrict, filename)
            filetype = match(regexnotstrict, filename)[1]
            warn("Saving output without extension is NOT raccomanded!")
        else
            error("Cannot determine file type from name, and filetype has not been specified!")
        end
    end
    ftype = get(HEADERTABLE, filetype, 0x00000000)
    if ftype == 0x00000000
        warn("Could not determine file type for file $filename. Using NULL headers ($ftype)")
    end
    fh = isgz(filename) ? GZip.open(filename, "w") : open(filename, "w+")
    write(fh, ftype)
    for raw in data
        write(fh, raw)
    end
    close(fh)   
end

function spikesbitidx(spikes)
    neurons = zeros(length(spikes[:,1]))
    ms =  length(spikes[:,1])
    spikeimg::Array{Int} = zeros(neurons, ms)
    for (i, y) in enumerate(spikes)
        diff = i % neurons
        nv = diff != 0 ? y * (neurons - diff) : y * neurons
        spikeimg[i] = nv
    end
    spikeimg
end

neuronsplit(arr, idx) = arr[idx,:]
neuronssplit(arr) = [ neuronsplit(arr, i) for i in 1:length(arr[:,1]) ]

## TODO: LABELS
"Plot raster of spikes of all neurons, in the defined ms range"
function spikesplot(arr::Array{Bool}, range::UnitRange; show = false, savename = "null") ## TODO: add spacing config
    p = plot()
    for (id, neuron) in enumerate(spikesIO.neuronssplit(arr))
        append!(p.layers, layer(x = range, y = (convert(Array{Int}, neuron[range] .+ id) - 0.5), Geom.line, Theme(default_color = Gadfly.distinguishable_colors(size(arr)[1])[id])))
    end
    show ? display(p) : "" ## FIXME!
    savename != "null" ? draw(SVGJS("$savename.svg", 30cm, 15cm), p) : ""
    p
end

function stimsplot(every::Int, arr::Array{Float32}, range::UnitRange; show = false, savename = "null")
    p = plot()
    for (id, neuron) in enumerate(spikesIO.neuronssplit(arr))
        append!(p.layers, layer(x = range, y = (convert(Array{Float32}, neuron[div(range, every)]*every)), Geom.line, Theme(default_color = Gadfly.distinguishable_colors(size(arr)[1])[id])))
    end
    show ? display(p) : "" ## FIXME
    savename != "null" ? draw(SVGJS("$savename.svg", 30cm, 15cm), p) : ""
    p
end

function angleplot(arr::Array{Float32}, range::UnitRange; show = false, savename = "null")
    p = plot()
    ## for (id, neuron) in enumerate(spikesIO.neuronssplit(arr))
    append!(p.layers, layer(x = range, y = arr, Geom.line, Theme(default_color = Gadfly.distinguishable_colors(size(arr)[1])[1])))
    ## end
    show ? display(p) : "" ## FIXME
    savename != "null" ? draw(SVGJS("$savename.svg", 30cm, 15cm), p) : ""
    p
end

## "Plot membrane potential"
## function membplot(arr, range)
##     p = plot()
##     for (id, neuron) in
##         color = distinguishable_colors(size(sim1)[3])[i]
##     plot(x = 1:30000, y = spikesIO.neuronspikes(membrane, 6), Geom.line)
## end

## "Analysis: Rossum spike train distance"
## function rossumdistance()
    
## end

end ## module

## image
# using Gadfly
# import DataFrames
# set_default_plot_size(30cm, 15cm)
# range = 1:1000
# plot(x = range, y = convert(Array{Int}, spikesIO.spikestosquare(spikesIO.neuronspikes(spikes, 5), time_window=15, threshold=0.10)[range] == true), Geom.line)
# plot(df[df[:ms] .< xlim, :], x="ms", y="neuron_id", color="state", Geom.rectbin)
# plot(x = range, y = spikesIO.runningmean(spikesIO.neuronspikes(spikes, 6), 30)[range], Geom.line)
