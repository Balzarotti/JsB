# Roadmap:
  * v0.1: - 2016-05-17
    v JsC: Add sha naming
    v config + cli: enable robot params
    v iSpike
  * v0.2: - 2016-05-18
    * TypeCheck and other performance tweak
    ~ Joint angle graphs
  * v0.3: - 2016-05-19
    ~ YARP: Add robot position/torque control (need config)
  * v0.4: - 2016-05-20
    * Various PID control examples
    ? PID config? (cerebellum could be a plugin)
      - This would mean batch-ability of various crb params!
  * undefined:
    * JsC: Add batch
